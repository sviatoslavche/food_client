// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA-lg1Fo1MtNf-aL0ajQ_1TZOtoDmvw_zM',
    authDomain: 'stravy-vid-mamy.firebaseapp.com',
    databaseURL: 'https://stravy-vid-mamy.firebaseio.com',
    projectId: 'stravy-vid-mamy',
    storageBucket: 'stravy-vid-mamy.appspot.com',
    messagingSenderId: '746051017548',
  },
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
