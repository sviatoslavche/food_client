import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { NgxPaginationModule } from 'ngx-pagination';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { FooterComponent } from './components/footer/footer.component';
import { FoodCardComponent } from './components/food-card/food-card.component';
import { MailComponent } from './pages/mail/mail.component';
import { MailBtnComponent } from './components/mail-btn/mail-btn.component';

import { FoodService } from './services/food/food.service';
import { CommonService } from './services/common/common.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MainLayoutComponent,
    HeaderComponent,
    AboutUsComponent,
    CategoriesComponent,
    FooterComponent,
    FoodCardComponent,
    MailComponent,
    MailBtnComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    ReactiveFormsModule,
    AngularFireAuthModule,
    FormsModule,
    HttpClientModule,
    SlickCarouselModule,
    NgxPaginationModule,
  ],
  providers: [FoodService, CommonService],
  bootstrap: [AppComponent],
})
export class AppModule {}
