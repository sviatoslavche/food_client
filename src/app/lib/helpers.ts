export function codeCollapsed(selector, selectorHidden, btnClick, container, btn, blockHeight, blockHeightMax) {
  const pre = $(selector);
  pre.each(function() {
    const self = $(this);
    const height = $(this).height();
    if (height > blockHeightMax) {
      self.wrap(container);
      self.parent().append(btn);
    } else {
      self.addClass('food_overflow--height');
    }
  });

  $(btnClick).on('click', function() {
    const scroll = $(window).scrollTop();
    const preheight = $(this)
      .siblings(selector)
      .height();
    if (
      $(this)
        .closest(selectorHidden)
        .hasClass('is-collapsed')
    ) {
      localStorage.setItem('myScroll', scroll as any);
      $(this)
        .closest(selectorHidden)
        .animate({ height: preheight + 15 }, 200)
        .removeClass('is-collapsed')
        .addClass('is-expanded');
    } else if (
      $(this)
        .closest(selectorHidden)
        .hasClass('is-expanded')
    ) {
      $(this)
        .closest(selectorHidden)
        .animate({ height: blockHeight }, 200)
        .removeClass('is-expanded')
        .addClass('is-collapsed');
      const getScroll = localStorage.getItem('myScroll');
      $('body,html').animate(
        {
          scrollTop: getScroll,
        },
        300,
      );
    }
  });
}
