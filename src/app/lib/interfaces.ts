export interface FoodData {
  id: any;
  images?: any;
  postAuthor: string;
  postAuthorUrl: string;
  postLastUpdate: number;
  postLink: string;
  postText: string;
  postTimeStamp: number;
  postType: string;
}

export interface PostResponse {
  from: number;
  posts: FoodData[];
  total: number;
}
