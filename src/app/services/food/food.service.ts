import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from 'src/app/lib/config';

@Injectable({
  providedIn: 'root',
})
export class FoodService {
  constructor(private httpClient: HttpClient) {}

  getFoods(reqData, successCb?: any, errorCb?: any) {
    this.httpClient.post(`${API_URL}/search/foodPost`, reqData, { responseType: 'json' }).subscribe(
      res => {
        if (successCb) {
          successCb(res);
        }
      },
      error => {
        if (errorCb) {
          errorCb(error);
        }
      },
    );
  }
}
