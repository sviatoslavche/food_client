import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { API_URL } from 'src/app/lib/config';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  mailModal = false;
  mailSubmitting = 'INIT';

  @Output() mailModalClosed: EventEmitter<any> = new EventEmitter<any>();

  constructor(private httpClient: HttpClient) {}

  toggleMailModal(): void {
    if (this.mailSubmitting === 'pending') {
      return;
    }

    this.mailModal = !this.mailModal;
    this.mailSubmitting = 'INIT';

    if (!this.mailModal) {
      this.mailModalClosed.emit();
    }
  }

  sendMail(reqData: any) {
    return this.httpClient.post(`${API_URL}/feedback/customer`, reqData, { responseType: 'json' });
  }
}
