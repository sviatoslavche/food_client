import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import qs from 'query-string';
import { FoodData } from 'src/app/lib/interfaces';
import { PageTitles } from 'src/app/lib/enums';
import { FoodService } from 'src/app/services/food/food.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  foods: FoodData[] = [];
  search = '';

  constructor(private foodService: FoodService, private router: Router, private title: Title) {}

  ngOnInit() {
    this.foodService.getFoods({ queryText: '', responseSize: 3, postType: 'offer' }, res => {
      this.foods = res.posts;
    });

    this.title.setTitle(PageTitles.home);
  }

  doSearch(): void {
    const param = {
      search: this.search !== '' ? this.search : undefined,
      type: 'offer',
    };

    const queryString = qs.stringify(param);

    this.router.navigateByUrl(`/categories?${queryString}`);
  }

  onKeyPress(event): void {
    const { key } = event;

    if (key === 'Enter') {
      this.doSearch();
    }
  }
}
