import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { PageTitles } from 'src/app/lib/enums';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss'],
})
export class AboutUsComponent implements OnInit {
  constructor(private title: Title) {}

  ngOnInit() {
    this.title.setTitle(PageTitles.aboutUs);
  }
}
