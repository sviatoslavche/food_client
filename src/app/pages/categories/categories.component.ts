import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { keys } from 'lodash';
import qs from 'query-string';
import { FoodData } from 'src/app/lib/interfaces';
import { PostType, PageSize, PageTitles } from 'src/app/lib/enums';
import { FoodService } from 'src/app/services/food/food.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent implements OnInit {
  foods: FoodData[] = [];
  params = {
    search: undefined,
    page: 1,
    type: '',
  };

  typeButtons = [
    { key: 'offer', value: 'offer', text: PostType.offer },
    { key: 'request', value: 'request', text: PostType.request },
    { key: 'review', value: 'review', text: PostType.review },
    { key: '', value: undefined, text: 'Всі' },
  ];

  PageSize = PageSize;

  totalPostCount = PageSize;

  constructor(private foodService: FoodService, private route: ActivatedRoute, private router: Router, private title: Title) {}

  ngOnInit() {
    this.route.queryParams.subscribe((params: any) => {
      const { search, page, type } = params;

      this.params = {
        search: search,
        page: page || 1,
        type: keys(PostType).indexOf(type) === -1 ? '' : type,
      };

      this.getFoods();
    });

    this.getFoods();

    this.title.setTitle(PageTitles.categories);
  }

  getFoods(): void {
    const { search, page, type } = this.params;

    this.foodService.getFoods({ queryText: search, from: PageSize * (page - 1), postType: type, responseSize: PageSize }, res => {
      this.foods = res.posts;
      this.params.page = page;
      this.totalPostCount = res.total;
    });
  }

  setPostType(type: any): void {
    const newParam = { ...this.params, type, page: 1 };
    this.redirect(qs.stringify(newParam));
  }

  doSearch(): void {
    if (this.params.search === '') {
      this.params.search = undefined;
    }

    this.redirect(qs.stringify(this.params));
  }

  onKeyPress(event): void {
    const { key } = event;
    if (key === 'Enter') {
      this.doSearch();
    }
  }

  pageChanged(event: number): void {
    const newParam = { ...this.params, page: event };
    this.redirect(qs.stringify(newParam));
  }

  redirect(queryString: string): void {
    this.router.navigateByUrl(`/categories?${queryString}`);
  }
}
