import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { PageTitles } from 'src/app/lib/enums';

@Component({
  selector: 'app-mail',
  templateUrl: './mail.component.html',
  styleUrls: ['./mail.component.scss'],
})
export class MailComponent implements OnInit {
  constructor(private title: Title) {}

  ngOnInit() {
    this.title.setTitle(PageTitles.mail);
  }
}
