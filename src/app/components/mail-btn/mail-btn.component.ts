import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common/common.service';

@Component({
  selector: 'app-mail-btn',
  templateUrl: './mail-btn.component.html',
  styleUrls: ['./mail-btn.component.scss'],
})
export class MailBtnComponent implements OnInit {
  constructor(public commonService: CommonService) {}

  ngOnInit() {}
}
