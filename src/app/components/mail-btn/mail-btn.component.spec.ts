import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailBtnComponent } from './mail-btn.component';

describe('MailBtnComponent', () => {
  let component: MailBtnComponent;
  let fixture: ComponentFixture<MailBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
