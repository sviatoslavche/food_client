import { Component, Input, OnInit, AfterViewInit } from '@angular/core';
import { get, values, omit } from 'lodash';
import { FoodData } from 'src/app/lib/interfaces';
import { codeCollapsed } from 'src/app/lib/helpers';
import { PostType } from 'src/app/lib/enums';

@Component({
  selector: 'app-food-card',
  templateUrl: './food-card.component.html',
  styleUrls: ['./food-card.component.scss'],
})
export class FoodCardComponent implements OnInit, AfterViewInit {
  @Input() data: FoodData = null;

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    codeCollapsed(
      `.food_overflow.food_overflow_${this.data.id}`,
      `.food_overflow--hidden.food_overflow_${this.data.id}--hidden`,
      `.food-more.food-more_${this.data.id}`,
      `<div class="food_overflow--hidden food_overflow_${this.data.id}--hidden is-collapsed"></div>`,
      `<div class="food-more food-more_${this.data.id}">Докладніше</div>`,
      '168',
      168,
    );
  }

  get postAuthorAvatar(): any {
    const authorPhoto = get(this.data.images, 'authorPhoto');

    if (authorPhoto) {
      return authorPhoto;
    }

    if (this.data.postType === 'offer') {
      return 'assets/img/home/offer-default.svg';
    }

    return 'assets/img/home/request-review-default.svg';
  }

  get hasImage(): boolean {
    return this.postImages.length > 0;
  }

  get postImages(): any {
    return values(omit(this.data.images, 'authorPhoto'));
  }

  get postLabel(): string {
    return PostType[this.data.postType];
  }
}
