import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/common/common.service';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  mailForm: FormGroup;

  constructor(public commonService: CommonService, private meta: Meta) {
    this.meta.addTag({ name: 'twitter:description', content: 'Платформа де можна замовити смачну домашню їжу від українських домогосподинь (м. Львів). Якщо ж Ви любите і вмієте готувати то тут ви можете заробити гроші на готуючи улюблені страви.' });
    this.meta.addTag({ name: 'keywords', content: 'страви від мами, домашня їжа, суші львів замовити, піца львів замовити, замовити їжу львів, страви файних господинь, львів кулінарний, їжа онлайн, їжа з доставкою, доставка їжі, суші доставка, піца доставка' });
    this.meta.addTag({ name: 'og:type', content: 'website' });
    this.meta.addTag({ name: 'og:title', content: 'Страви Від Мами - Смачна домашня їжа від українських домогосподинь' });
    this.meta.addTag({ name: 'og:description', content: 'Платформа де можна замовити смачну домашню їжу від українських домогосподинь (м. Львів). Якщо ж Ви любите і вмієте готувати то тут ви можете заробити гроші на готуючи улюблені страви.' });

    this.meta.addTag({ name: 'og:url', content: 'https://stravy-vid-mamy.in.ua' });
    this.meta.addTag({ name: 'og:image', content: 'https://stravy-vid-mamy.in.ua/img/fb-baner.png' });
    this.meta.addTag({ name: 'og:image:width', content: '1200' });
    this.meta.addTag({ name: 'og:image:height', content: '526' });
  }

  ngOnInit() {
    this.mailForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      mail: new FormControl('', [Validators.required, Validators.email]),
      message: new FormControl('', [Validators.required, Validators.maxLength(500)]),
    });

    this.commonService.mailModalClosed.subscribe(() => {
      this.mailForm.reset();
    });
  }

  get name() {
    return this.mailForm.get('name');
  }

  get mail() {
    return this.mailForm.get('mail');
  }

  get message() {
    return this.mailForm.get('message');
  }

  onSubmit(): void {
    this.commonService.mailSubmitting = 'pending';

    this.commonService.sendMail(this.mailForm.value).subscribe(
      () => {
        this.commonService.mailSubmitting = 'success';
      },
      () => {
        this.commonService.mailSubmitting = 'fail';
      },
    );
  }
}
