(function() {
  window.addEventListener('load', readF);
  function readF() {
    function inPage(selector) {
      return document.querySelector(selector);
    }

    if (inPage('.mail_btn')) {
      const mailBtn = document.querySelector('.mail_btn');
      const formBlock = document.querySelector('.form_block');
      const formBlockBg = document.querySelector('.form_block__bg');
      const formBlockClose = document.querySelector('.form_close');

      mailBtn.addEventListener('click', function() {
        formBlock.classList.add('form_block--active');
      });
      formBlockBg.addEventListener('click', function() {
        formBlock.classList.remove('form_block--active');
      });
      formBlockClose.addEventListener('click', function() {
        formBlock.classList.remove('form_block--active');
      });
    }

    const btnNav = document.querySelector('.btn_nav');
    const nav = document.querySelector('.nav');

    btnNav.addEventListener('click', function() {
      btnNav.classList.toggle('btn_nav--active');
      nav.classList.toggle('active');
    });

    function closeNav() {
      btnNav.classList.remove('btn_nav--active');
      nav.classList.remove('active');
    }

    window.addEventListener('scroll', function() {
      if (this.pageYOffset > 0) {
        closeNav();
      }
    });

    $('.nav a').on('click', function() {
      closeNav();
    });

    function notClick(selector, elem, removeClass) {
      $(document).on('click', function(e) {
        if (!$(e.target).closest(selector).length) {
          $(elem).removeClass(removeClass);
          btnNav.classList.remove('btn_nav--active');
        }
        e.stopPropagation();
      });
    }
    notClick('.header', '.nav', 'active');
  }
})();
